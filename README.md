# DoItApp

## Introducción
Esta aplicación android cumple la funcion de una todolist, con ella serás capaz de organizar tu día a día con una serie de tareas que podrás administrar tú mismo.

## Activities

### activity_main
Esta activity es la principal pero solo sirve de llave de paso, ya que carga al empezar automáticamente los fragments correspondientes.

### item_recyclerview.xml y list_recyclerview.xml
En estos recyclerviews encontramos como se van a posicionar los datos, ya sean items o listas. El tamaño y el color que tendran. 

### fragment_items.xml y fragment_list.xml
En estos fragments entontramos como se van a posicionar los recyclerviews de items y lists respectivamente. 
Tambien encontramos la posicion de los botones de añadir asi como una celda para escribir el dato a añadir.

## Services
Dentro de las funciones de servicios, las clases java que encontramos son:
### Main
Packages como:
### Adapter
En este package encontramos los adapters de List y Task.
Los cuales recogen los datos y crea la vista para cada lista/task individualmente.
### Fragments
En este package encontramos los fragments de List y Task y sus recyclerviews de estos.
### Models
En este package encontramos las clases List y Task los cuales contienen los atributos de una lista y de cada task.
También se hayan el ViewModel de lista y task, donde encontraremos los datos de las listas y tasks.
### OnClickListener
Estas interfaces sirven para recuperar la lista o la tarea que se esta clickando en ese momento.
### Repository
En esta clase recibimos las respuestas de la API y las pasamos a las clases que nos las piden o devolvemos un error si no ha podido procesar la petición.
### Api
En esta interfaz hacemos las conexiones con la API con las distintas peticiones que necestiamos.
### Jsontodata
Estas clases de datos son objetos que guardan la información que proviene de la API.

## Dispositivo
Esta app esta desarrollada para un dispositivo android con api desde 21 hasta 31.
