package com.example.doit.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.doit.OnClickListenerTask
import com.example.doit.R
import com.example.doit.databinding.TaskRecyclerviewBinding
import com.example.doit.jsondata.Items

class TaskAdapter(private val tasks: List<Items>, private val listener: OnClickListenerTask, context: Context?):
    RecyclerView.Adapter<TaskAdapter.ViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.task_recyclerview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        with(holder) {
            setListener(task)
            binding.description.text = task.description
            if (!task.stat) {
                binding.description.setBackgroundColor(ContextCompat.getColor(context, R.color.secondary))
            } else {
                binding.description.setBackgroundColor(ContextCompat.getColor(context, R.color.primary))
            }
        }

    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = TaskRecyclerviewBinding.bind(view)

        fun setListener(task: Items){
            binding.root.setOnClickListener {
                listener.onClick(task)
            }
        }
        
    }

}