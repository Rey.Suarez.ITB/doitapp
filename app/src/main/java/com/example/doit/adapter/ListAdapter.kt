package com.example.doit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doit.OnClickListenerList
import com.example.doit.R
import com.example.doit.databinding.ListRecyclerviewBinding
import com.example.doit.jsondata.Lists

class ListAdapter(private val lists: List<Lists>, private val listener: OnClickListenerList):
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.list_recyclerview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = lists[position]
        with(holder) {
            setListener(list)
            binding.listname.text = list.name
        }

    }
    override fun getItemCount(): Int {
        return lists.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ListRecyclerviewBinding.bind(view)

        fun setListener(list: Lists){
            binding.root.setOnClickListener {
                listener.onClick(list)
            }
        }
    }

}