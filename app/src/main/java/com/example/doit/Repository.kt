package com.example.doit

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.doit.api.ApiInterface
import com.example.doit.jsondata.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    private val apiInterface = ApiInterface.create()
    var listsInfo = MutableLiveData<DataLists>();
    var itemsInfo = MutableLiveData<DataItems>();

//    fun fetchLists(url: String): MutableLiveData<DataLists> {
//        val call = apiInterface.getDataLists(url)
//        call.enqueue(object: Callback<DataLists> {
//            override fun onFailure(call: Call<DataLists>, t: Throwable) {
//                Log.e("ERROR", "HORROR: ${t.message.toString()}")
//                listsInfo.postValue(null)
//            }
//            override fun onResponse(call: Call<DataLists>, response: Response<DataLists>) {
//                if (response != null && response.isSuccessful) {
//                    listsInfo.value = response.body()
//                }
//                listsInfo.value = response.body()
//            }
//        })
//        return listsInfo
//    }
//
//    fun fetchItems(url: String): MutableLiveData<DataItems> {
//        val call = apiInterface.getDataItems(url)
//        call.enqueue(object: Callback<DataItems>{
//            override fun onFailure(call: Call<DataItems>, t: Throwable) {
//                Log.e("ERROR", t.message.toString())
//                itemsInfo.postValue(null)
//            }
//            override fun onResponse(call: Call<DataItems>, response: Response<DataItems>) {
//                if (response != null && response.isSuccessful) {
//                    itemsInfo.value = response.body()
//                }
//                itemsInfo.value = response.body()
//            }
//        })
//        return itemsInfo
//    }
//
//    fun postLists(list: ListsPost) {
//        val call = apiInterface.postDataLists(list)
//        call.enqueue(object: Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Log.e("ERROR", "La API no es troba disponible o no es pot crear la llista.")
//            }
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                Log.e("CREAT", "L'usuari ha set creat.")
//            }
//        })
//    }
//
//    fun postItems(item: ItemsPost, idList: Int) {
//        val call = apiInterface.postDataItems(idList, item)
//        call.enqueue(object: Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Log.e("ERROR", "La API no es troba disponible o no es pot crear la tasca.")
//            }
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                Log.e("CREAT", "La tasca s'ha creat.")
//            }
//        })
//    }
//
//    fun updateLists(list: ListsPost, idList: Int) {
//        val call = apiInterface.updateDataLists(idList, list)
//        call.enqueue(object: Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Log.e("ERROR", "La API no es troba disponible o no es pot crear la llista.")
//            }
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                Log.e("CREAT", "L'usuari s'ha actualitzat.")
//            }
//        })
//    }
//
//    fun updateItems(item: ItemsPost, idItem: Int, idList: Int) {
//        val call = apiInterface.updateDataItems(idList, idItem, item)
//        call.enqueue(object: Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Log.e("ERROR", "La API no es troba disponible o no es pot crear la llista.")
//            }
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                Log.e("CREAT", "L'usuari s'ha actualitzat.")
//            }
//        })
//    }
//
//    fun deleteLists(idList: Int) {
//        val call = apiInterface.deleteDataLists(idList)
//        call.enqueue(object: Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Log.e("ERROR", "La API no es troba disponible o no es pot esborrar la llista.")
//            }
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                Log.e("ESBORRAT", "La llista s'ha esborrat.")
//            }
//        })
//    }
//
//    fun deleteItems(idItem: Int) {
//        val call = apiInterface.deleteDataItems(idItem)
//        call.enqueue(object: Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//                Log.e("ERROR", "La API no es troba disponible o no es pot esborrar la tasca.")
//            }
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//                Log.e("ESBORRAT", "La tasca s'ha esborrat.")
//            }
//        })
//    }

    suspend fun fetchLists(url: String) = apiInterface.getDataLists(url)

    suspend fun fetchItems(url: String) = apiInterface.getDataItems(url)

    suspend fun postLists(list: ListsPost) = apiInterface.postDataLists(list)

    suspend fun postItems(item: ItemsPost, idList: Int) = apiInterface.postDataItems(idList, item)

    suspend fun updateLists(list: ListsPost, idList: Int) = apiInterface.updateDataLists(idList, list)

    suspend fun updateItems(item: ItemsPost, idItem: Int, idList: Int) = apiInterface.updateDataItems(idList, idItem, item)

    suspend fun deleteLists(idList: Int) = apiInterface.deleteDataLists(idList)

    suspend fun deleteItems(idItem: Int) = apiInterface.deleteDataItems(idItem)

}
