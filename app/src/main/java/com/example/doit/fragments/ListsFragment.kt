package com.example.doit.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_NULL
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.doit.OnClickListenerList
import com.example.doit.R
import com.example.doit.SwipeGesture
import com.example.doit.adapter.ListAdapter
import com.example.doit.databinding.FragmentListsBinding
import com.example.doit.jsondata.DataLists
import com.example.doit.jsondata.Lists
import com.example.doit.jsondata.ListsPost
import com.example.doit.model.ListViewModel
import com.example.doit.model.TaskViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListsFragment:Fragment(), OnClickListenerList {

    private lateinit var listAdapter: ListAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentListsBinding
    private val model: ListViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentListsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshApp()

        model.dataLists.observe(viewLifecycleOwner, {
            if (model.dataLists.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            }
            else{
                setUpRecyclerView(it)
            }
        })

        binding.addNewList.setOnClickListener{
            if (!binding.translucentBackground.isVisible) {
                val newNameList: String = binding.editText.text.toString()
                val newList = ListsPost(0, newNameList)
                model.createList(newList)
                hideKeyboard(it)
                binding.editText.text = null
            }
        }
    }

    private fun setUpRecyclerView(myData: DataLists) {
        listAdapter = ListAdapter(myData, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerViewList.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = listAdapter
        }

        swipeFunction()

    }

    private fun swipeFunction() {

        val swipegesture = object : SwipeGesture(context) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when(direction){

                    ItemTouchHelper.LEFT ->{

                        //Función borrar

                        model.deleteList(viewHolder.adapterPosition)

                    }

                    ItemTouchHelper.RIGHT ->{

                        //Función editar

                        val view = viewHolder.adapterPosition
                        binding.editText.isFocusable = false
                        binding.editText.isFocusableInTouchMode = false
                        binding.editText.inputType = TYPE_NULL
                        binding.translucentBackground.visibility = View.VISIBLE
                        binding.swipeToRefresh.isEnabled = false
                        binding.editName.visibility = View.VISIBLE
                        if (binding.editName.isVisible) {
                            binding.updateNameList.setOnClickListener() {
                                model.updateList(binding.newNameList.text.toString(), view)
                                binding.newNameList.text = null
                                binding.editText.isFocusable = true
                                binding.editText.isFocusableInTouchMode = true
                                binding.editText.inputType = InputType.TYPE_CLASS_TEXT
                                binding.translucentBackground.visibility = View.GONE
                                binding.editName.visibility = View.GONE
                                binding.swipeToRefresh.isEnabled = true
                                hideKeyboard(it)
                            }
                        }
                    }

                }


            }

        }

        val touchHelper = ItemTouchHelper(swipegesture)
        touchHelper.attachToRecyclerView(binding.recyclerViewList)
    }

    private fun refreshApp() {
        binding.swipeToRefresh.setOnRefreshListener {
            model.fetchList("todoList")
            Toast.makeText(context, "Pàgina refrescada", Toast.LENGTH_SHORT).show()
            binding.swipeToRefresh.isRefreshing = false
        }
    }

    private fun hideKeyboard(view: View) {
        view.apply {
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onClick(list: Lists) {
        if (!binding.translucentBackground.isVisible) {
            model.setList(list)
            val modelTasks: TaskViewModel by activityViewModels()
            modelTasks.changeIdListItem(list.id)
            modelTasks.nameList = list.name
            view?.let {
                Navigation.findNavController(it).navigate(R.id.action_listsFragment2_to_tasksFragment2)
            }
        }
    }

}