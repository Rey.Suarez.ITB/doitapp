package com.example.doit.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.doit.R
import com.example.doit.databinding.FragmentInitialBinding

class InitialFragment:Fragment() {

    private lateinit var binding: FragmentInitialBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentInitialBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.list.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_initialFragment_to_listsFragment2)
        }

    }

}