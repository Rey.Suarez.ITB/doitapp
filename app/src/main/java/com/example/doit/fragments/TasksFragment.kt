package com.example.doit.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_NULL
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.doit.OnClickListenerTask
import com.example.doit.R
import com.example.doit.adapter.TaskAdapter
import com.example.doit.databinding.FragmentTasksBinding
import com.example.doit.jsondata.*
import com.example.doit.model.TaskViewModel
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.doit.SwipeGesture
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TasksFragment:Fragment(), OnClickListenerTask {

    private lateinit var taskAdapter: TaskAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentTasksBinding
    private val model: TaskViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTasksBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshApp()

        model.dataItems.observe(viewLifecycleOwner, Observer {
            if (model.dataItems.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            }
            else{
                setUpRecyclerView(it)
            }
        })

        binding.addNewItem.setOnClickListener{

            val newDescriptionItem: String = binding.editText.text.toString()
            val newList = ItemsPost(newDescriptionItem, 0, 5, false)
            model.createItem(newList)
            hideKeyboard(it)
            binding.editText.text = null

        }

        binding.list.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_tasksFragment2_to_listsFragment2)
        }

        binding.listNameView?.text = model.nameList
    }

    private fun refreshApp() {
        binding.swipeToRefresh.setOnRefreshListener {
            model.fetchItem()
            Toast.makeText(context, "Pàgina refrescada", Toast.LENGTH_SHORT).show()
            binding.swipeToRefresh.isRefreshing = false
        }
    }

    private fun setUpRecyclerView(myData: DataItems) {
        taskAdapter = TaskAdapter(myData, this, context)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerViewTask.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = taskAdapter
        }

        swipeFunction()

    }

    private fun swipeFunction() {

        val swipegesture = object : SwipeGesture(context) {

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when(direction){

                    ItemTouchHelper.LEFT ->{

                        //Función borrar

                        model.deleteItem(viewHolder.adapterPosition)

                    }

                    ItemTouchHelper.RIGHT ->{

                        //Función editar

                        val view = viewHolder.adapterPosition
                        binding.editText.isFocusable = false
                        binding.editText.isFocusableInTouchMode = false
                        binding.editText.inputType = TYPE_NULL
                        binding.translucentBackground.visibility = View.VISIBLE
                        binding.swipeToRefresh.isEnabled = false
                        binding.editDescription.visibility = View.VISIBLE
                        if (binding.editDescription.isVisible) {
                            binding.updateDescriptionItem.setOnClickListener() {
                                model.updateItemDescription(binding.newDescriptionItem.text.toString(), view)
                                binding.newDescriptionItem.text = null
                                binding.editText.isFocusable = true
                                binding.editText.isFocusableInTouchMode = true
                                binding.editText.inputType = InputType.TYPE_CLASS_TEXT
                                binding.translucentBackground.visibility = View.GONE
                                binding.editDescription.visibility = View.GONE
                                binding.swipeToRefresh.isEnabled = true
                                hideKeyboard(it)
                            }
                        }

                    }

                }
            }

        }

        val touchHelper = ItemTouchHelper(swipegesture)
        touchHelper.attachToRecyclerView(binding.recyclerViewTask)

    }

    private fun hideKeyboard(view: View) {
        view?.apply {
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onClick(task: Items) {

        if (!task.stat) {
            model.updateItemState(task, true)
        } else {
            model.updateItemState(task, false)
        }

    }

}