package com.example.doit.model

data class Task(val id: Int, var description: String, var status: Boolean, var priority: Int, val list: Int)