package com.example.doit.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.doit.Repository
import com.example.doit.jsondata.DataLists
import com.example.doit.jsondata.Lists
import com.example.doit.jsondata.ListsPost
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListViewModel: ViewModel(){

    val repository = Repository()
    var dataLists = MutableLiveData<DataLists>()
    var actualList = MutableLiveData<Lists>()

    init {
        fetchList("todoList")
    }

    fun fetchList(url: String){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){repository.fetchLists(url)}
            if(response.isSuccessful){
                dataLists.postValue(response.body())
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }

    fun createList(list: ListsPost){
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.postLists(list)}
            fetchList("todoList")
        }
    }

    fun deleteList(pos: Int){
        val listMod = dataLists.value!![pos]
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.deleteLists(listMod.id)}
            fetchList("todoList")
        }
    }

    fun updateList(editName: String, pos: Int){
        val listMod = ListsPost(dataLists.value!![pos].id, editName)
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.updateLists(listMod, listMod.id)}
            fetchList("todoList")
        }
    }

    fun setList(list: Lists){
        actualList.value = list
    }

}