package com.example.doit.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.doit.Repository
import com.example.doit.jsondata.DataItems
import com.example.doit.jsondata.Items
import com.example.doit.jsondata.ItemsPost
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TaskViewModel: ViewModel(){

    val repository = Repository()
    var dataItems = MutableLiveData<DataItems>()
    var initialListItems = 1
    var nameList = ""
    var url = "todoList/1/todoItem"

    init {
        fetchItem()
    }

    fun fetchItem(){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){repository.fetchItems(url)}
            if(response.isSuccessful){
                dataItems.postValue(response.body())
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }

    fun createItem(item: ItemsPost){
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.postItems(item, initialListItems)}
            fetchItem()
        }
    }

    fun deleteItem(pos: Int){
        val itemMod = dataItems.value!![pos]
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.deleteItems(itemMod.id)}
            fetchItem()
        }
    }

    fun updateItemDescription(editDescription: String, pos: Int){
        val itemMod = ItemsPost(editDescription, dataItems.value!![pos].id, dataItems.value!![pos].priority, dataItems.value!![pos].stat)
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.updateItems(itemMod, itemMod.id, initialListItems)}
            fetchItem()
        }
    }

    fun updateItemState(item: Items, editState: Boolean){
        val itemMod = ItemsPost(item.description, item.id, item.priority, editState)
        viewModelScope.launch {
            withContext(Dispatchers.IO){repository.updateItems(itemMod, itemMod.id, initialListItems)}
            fetchItem()
        }
    }

    fun changeIdListItem(list: Int) {
        initialListItems = list
        url = "todoList/"+initialListItems+"/todoItem"
        fetchItem()
    }

}