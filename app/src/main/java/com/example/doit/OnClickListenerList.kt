package com.example.doit

import com.example.doit.jsondata.Lists

interface OnClickListenerList {
    fun onClick(list: Lists)
}