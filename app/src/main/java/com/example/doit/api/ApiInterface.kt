package com.example.doit.api

import com.example.doit.jsondata.*
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @GET()
    suspend fun getDataLists(@Url url: String): Response<DataLists>

    @GET()
    suspend fun getDataItems(@Url url: String): Response<DataItems>

    @POST("todoList")
    suspend fun postDataLists(@Body listData: ListsPost): Response<ResponseBody>

    @POST("todoList/{id}/todoItem")
    suspend fun postDataItems(@Path("id") idList: Int, @Body itemData: ItemsPost): Response<ResponseBody>

    @POST("todoList/{id}")
    suspend fun updateDataLists(@Path("id") idList: Int, @Body listData: ListsPost): Response<ResponseBody>

    @POST("todoList/{idList}/todoItem/{idItem}")
    suspend fun updateDataItems(@Path("idList") idList: Int, @Path("idItem") idItem: Int, @Body itemData: ItemsPost): Response<ResponseBody>

    @DELETE("todoList/{id}")
    suspend fun deleteDataLists(@Path("id") idList: Int): Response<ResponseBody>

    @DELETE("todoList/todoItem/{id}")
    suspend fun deleteDataItems(@Path("id") idItem: Int): Response<ResponseBody>

    companion object {
        val BASE_URL = "https://apidoit.herokuapp.com/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }

}