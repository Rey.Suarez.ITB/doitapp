package com.example.doit.jsondata

data class ListsPost(
    var id: Int,
    var name: String
)