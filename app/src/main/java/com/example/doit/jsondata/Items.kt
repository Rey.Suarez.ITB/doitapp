package com.example.doit.jsondata

data class Items(
    val description: String,
    val id: Int,
    val list: Int,
    val priority: Int,
    val stat: Boolean
)