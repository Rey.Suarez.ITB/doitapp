package com.example.doit.jsondata

data class ItemsPost(
    var description: String,
    var id: Int,
    var priority: Int,
    var stat: Boolean
)