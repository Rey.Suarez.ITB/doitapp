package com.example.doit

import androidx.recyclerview.widget.RecyclerView
import com.example.doit.jsondata.Items
import com.example.doit.model.Task

interface OnClickListenerTask {
    fun onClick(task: Items)
}